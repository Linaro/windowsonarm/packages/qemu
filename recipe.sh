#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" 1>&2
    exit 1
}

[ $# -eq 2 ] || die "usage: revision out_dir"
revision=$1; shift
out_dir=$1; shift

set -euo pipefail

exec_msys_arm64()
{
    # execute as login (-l) allows to setup CLANGARM64 env
    # force through cmd.exe to nest out of current msys2 env
    cmd.exe /c "set MSYSTEM=CLANGARM64 && bash.exe -lc \"env PATH=/clangarm64/bin:\$PATH $*\""
    cmd.exe /c "taskkill /IM gpg-agent.exe /f" || true
}

setup_msys_arm64()
{
    local url="https://github.com/msys2/msys2-installer/releases/download/2022-12-16/msys2-base-x86_64-20221216.sfx.exe"
    wget -q $url -O msys2.exe
    ./msys2.exe
    # avoid clear screen on first connect
    sed -e '/clear/d' -i msys64/etc/profile
    cat >> msys64/etc/pacman.conf << EOF
SigLevel = Never
[clangarm64]
Include = /etc/pacman.d/mirrorlist.mingw
EOF
    exec_msys_arm64 pacman-key --init
    exec_msys_arm64 pacman-key --populate
    exec_msys_arm64 pacman -Sy
    exec_msys_arm64 pacman -S base-devel --noconfirm
}

checkout()
{
    local revision=$1
    if [ -d qemu ]; then
        return
    fi
    git clone https://gitlab.com/qemu-project/qemu
    pushd qemu
    git checkout $revision
    git log -n1

    popd
}

get_dependencies()
{
    done=deps/.done
    export PATH=$(pwd)/deps/msys64/usr/bin:$PATH

    if [ -f $done ]; then
        return
    fi

    rm -rf deps
    mkdir -p deps
    pushd deps

    setup_msys_arm64
    # deps from https://github.com/msys2/MINGW-packages/blob/master/mingw-w64-qemu/PKGBUILD
    export MINGW_PACKAGE_PREFIX=mingw-w64-clang-aarch64
makedepends=(
  "${MINGW_PACKAGE_PREFIX}-meson"
  "${MINGW_PACKAGE_PREFIX}-ninja"
  "${MINGW_PACKAGE_PREFIX}-python"
  "${MINGW_PACKAGE_PREFIX}-python-sphinx"
  "${MINGW_PACKAGE_PREFIX}-python-sphinx_rtd_theme"
  "${MINGW_PACKAGE_PREFIX}-autotools"
  "${MINGW_PACKAGE_PREFIX}-tools-git"
  "${MINGW_PACKAGE_PREFIX}-cc")
depends=(
  "${MINGW_PACKAGE_PREFIX}-capstone"
  "${MINGW_PACKAGE_PREFIX}-curl"
  "${MINGW_PACKAGE_PREFIX}-cyrus-sasl"
  "${MINGW_PACKAGE_PREFIX}-expat"
  "${MINGW_PACKAGE_PREFIX}-fontconfig"
  "${MINGW_PACKAGE_PREFIX}-freetype"
  "${MINGW_PACKAGE_PREFIX}-fribidi"
  "${MINGW_PACKAGE_PREFIX}-gcc-libs"
  "${MINGW_PACKAGE_PREFIX}-gdk-pixbuf2"
  "${MINGW_PACKAGE_PREFIX}-gettext"
  "${MINGW_PACKAGE_PREFIX}-glib2"
  "${MINGW_PACKAGE_PREFIX}-gmp"
  "${MINGW_PACKAGE_PREFIX}-gnutls"
  "${MINGW_PACKAGE_PREFIX}-graphite2"
  "${MINGW_PACKAGE_PREFIX}-gst-plugins-base"
  "${MINGW_PACKAGE_PREFIX}-gstreamer"
  "${MINGW_PACKAGE_PREFIX}-gtk3"
  "${MINGW_PACKAGE_PREFIX}-harfbuzz"
  "${MINGW_PACKAGE_PREFIX}-jbigkit"
  "${MINGW_PACKAGE_PREFIX}-lerc"
  "${MINGW_PACKAGE_PREFIX}-libc++"
  "${MINGW_PACKAGE_PREFIX}-libdatrie"
  "${MINGW_PACKAGE_PREFIX}-libdeflate"
  "${MINGW_PACKAGE_PREFIX}-libepoxy"
  "${MINGW_PACKAGE_PREFIX}-libffi"
  "${MINGW_PACKAGE_PREFIX}-libiconv"
  "${MINGW_PACKAGE_PREFIX}-libidn2"
  "${MINGW_PACKAGE_PREFIX}-libjpeg-turbo"
  "${MINGW_PACKAGE_PREFIX}-libnfs"
  "${MINGW_PACKAGE_PREFIX}-libpng"
  "${MINGW_PACKAGE_PREFIX}-libpsl"
  "${MINGW_PACKAGE_PREFIX}-libslirp"
  "${MINGW_PACKAGE_PREFIX}-libssh"
  "${MINGW_PACKAGE_PREFIX}-libssh2"
  "${MINGW_PACKAGE_PREFIX}-libtasn1"
  "${MINGW_PACKAGE_PREFIX}-libthai"
  "${MINGW_PACKAGE_PREFIX}-libtiff"
  "${MINGW_PACKAGE_PREFIX}-libunistring"
  "${MINGW_PACKAGE_PREFIX}-libunwind"
  "${MINGW_PACKAGE_PREFIX}-libusb"
  "${MINGW_PACKAGE_PREFIX}-libwebp"
  "${MINGW_PACKAGE_PREFIX}-libwinpthread-git"
  "${MINGW_PACKAGE_PREFIX}-lz4"
  "${MINGW_PACKAGE_PREFIX}-lzo2"
  "${MINGW_PACKAGE_PREFIX}-nettle"
  "${MINGW_PACKAGE_PREFIX}-openssl"
  "${MINGW_PACKAGE_PREFIX}-opus"
  "${MINGW_PACKAGE_PREFIX}-orc"
  "${MINGW_PACKAGE_PREFIX}-p11-kit"
  "${MINGW_PACKAGE_PREFIX}-pango"
  "${MINGW_PACKAGE_PREFIX}-pcre"
  "${MINGW_PACKAGE_PREFIX}-pixman"
  "${MINGW_PACKAGE_PREFIX}-SDL2"
  "${MINGW_PACKAGE_PREFIX}-SDL2_image"
  "${MINGW_PACKAGE_PREFIX}-snappy"
  "${MINGW_PACKAGE_PREFIX}-spice"
  "${MINGW_PACKAGE_PREFIX}-usbredir"
  "${MINGW_PACKAGE_PREFIX}-xz"
  "${MINGW_PACKAGE_PREFIX}-zlib"
  "${MINGW_PACKAGE_PREFIX}-zstd"
)
    exec_msys_arm64 pacman -S --noconfirm "${makedepends[@]}" "${depends[@]}"
    exec_msys_arm64 pacman -S --noconfirm \
        git ninja python ${MINGW_PACKAGE_PREFIX}-cc
    popd
    touch $done
}

build()
{
    pushd qemu
    if [ ! -f build/.configured ]; then
        # issue with spice headers
        export CFLAGS='-Wno-pragma-pack'
        exec_msys_arm64 ./configure --disable-docs
        touch build/.configured
    fi
    exec_msys_arm64 ninja -C build
    exec_msys_arm64 make check || true
    popd
}

checkout $revision
get_dependencies
build
